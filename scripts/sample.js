var sample;
(function (sample) {
    //================================================================
    // ãƒ—ãƒ­ãƒ‘ãƒ†ã‚£ãƒ‘ãƒãƒ«
    //================================================================
    var PropertyPanel = /** @class */ (function () {
        function PropertyPanel(element, control, options) {
            _tabpanel = new wijmo.nav.TabPanel(element);
            wijmo.addClass(_tabpanel.hostElement, 'wj-property-panel');
            for (var _i = 0, options_1 = options; _i < options_1.length; _i++) {
                var category = options_1[_i];
                var header = document.createElement('a');
                header.textContent = category.category;
                var pane = document.createElement('div');
                pane.className = 'wj-property-pane';
                for (var _a = 0, _b = category.props; _a < _b.length; _a++) {
                    var prop = _b[_a];
                    var label = document.createElement('label');
                    if (prop && prop.title) {
                        label.innerHTML = prop.type == 'Button' ? null : prop.title;
                        label.appendChild(this.createInput(control, prop));
                        if (prop.full) {
                            label.className = 'full-width';
                        }
                    }
                    pane.appendChild(label);
                }
                _tabpanel.tabs.push(new wijmo.nav.Tab(header, pane));
            }
        }
        PropertyPanel.prototype.createInput = function (control, prop) {
            var input;
            if (prop.type == 'Boolean') {
                input = document.createElement('input');
                input.type = 'checkbox';
                input.checked = prop.value ? prop.value : getDeepProperty(control, prop.name) == true;
                input.addEventListener('change', prop.changed ? prop.changed : function (e) {
                    setDeepProperty(control, prop.name, e.target.checked);
                });
            }
            else if (prop.type == 'String') {
                input = document.createElement('input');
                input.value = prop.value ? prop.value : getDeepProperty(control, prop.name);
                input.addEventListener('input', prop.changed ? prop.changed : function (e) {
                    setDeepProperty(control, prop.name, e.target.value);
                });
            }
            else if (prop.type == 'Button') {
                input = document.createElement('button');
                input.innerHTML = prop.title;
                input.className = 'btn btn-default';
                input.addEventListener('click', prop.changed);
            }
            else if (prop.type == 'Number') {
                input = document.createElement('div');
                new wijmo.input.InputNumber(input, {
                    isRequired: false,
                    value: prop.value ? prop.value : getDeepProperty(control, prop.name),
                    step: prop.step ? prop.step : 1,
                    valueChanged: prop.changed ? prop.changed : function (s, e) {
                        setDeepProperty(control, prop.name, s.value);
                    }
                });
            }
            else if (prop.type == 'Date') {
                input = document.createElement('div');
                new wijmo.input.InputDate(input, {
                    isRequired: false,
                    value: prop.value ? prop.value : getDeepProperty(control, prop.name),
                    valueChanged: prop.changed ? prop.changed : function (s, e) {
                        setDeepProperty(control, prop.name, s.value);
                    }
                });
            }
            else if (prop.type == 'Time') {
                input = document.createElement('div');
                new wijmo.input.InputTime(input, {
                    isRequired: false,
                    step: 60,
                    value: prop.value ? prop.value : getDeepProperty(control, prop.name),
                    valueChanged: prop.changed ? prop.changed : function (s, e) {
                        setDeepProperty(control, prop.name, s.value);
                    }
                });
            }
            else if (prop.type == 'Array') {
                input = document.createElement('div');
                new wijmo.input.ComboBox(input, {
                    itemsSource: sample.keyValues(prop.keys, prop.values),
                    displayMemberPath: 'name',
                    selectedValuePath: 'value',
                    selectedValue: prop.value ? prop.value : getDeepProperty(control, prop.name),
                    selectedIndexChanged: prop.changed ? prop.changed : function (s, e) {
                        setDeepProperty(control, prop.name, s.selectedValue);
                    }
                });
            }
            if (prop.value) {
                setDeepProperty(control, prop.name, prop.value);
            }
            input.id = prop.name;
            return input;
        };
        return PropertyPanel;
    }());
    sample.PropertyPanel = PropertyPanel;
    function getDeepProperty(control, name) {
        var property = control;
        for (var _i = 0, _a = name.split('.'); _i < _a.length; _i++) {
            _name = _a[_i];
            property = property[_name];
        }
        return property;
    }
    function setDeepProperty(control, name, value) {
        var property = control;
        var names = name.split('.');
        var lastName = names.pop();
        for (var _i = 0, names_1 = names; _i < names_1.length; _i++) {
            _name = names_1[_i];
            property = property[_name];
        }
        property[lastName] = value;
    }
    //================================================================
    // å…±é€š
    //================================================================
    sample.showLogs = true;
    sample.path = 'http://demo.grapecity.com/wijmo/sample/';
    var _currencyRate = 100;
    var _dateFormat = '{0}å¹´åº¦';
    var _years = 5;
    function _firstYear(ago) {
        if (ago === void 0) { ago = _years; }
        return addMonths(new Date(), -3).getFullYear() - ago;
    }
    var _suspended = false;
    function _log(data, name) {
        if (sample.showLogs && !_suspended) {
            console.log('** ' + name + ' **', data.slice(0, 100));
        }
    }
    function _suspendLog() {
        _suspended = true;
    }
    function _resumeLog() {
        _suspended = false;
    }
    //================================================================
    // å—æ³¨
    //================================================================
    var OrderRaw = /** @class */ (function () {
        function OrderRaw() {
        }
        return OrderRaw;
    }());
    sample.OrderRaw = OrderRaw;
    var Order = /** @class */ (function () {
        function Order() {
        }
        return Order;
    }());
    sample.Order = Order;
    var OrderDetail = /** @class */ (function () {
        function OrderDetail() {
        }
        return OrderDetail;
    }());
    sample.OrderDetail = OrderDetail;
    function ordersRaw(length) {
        if (length === void 0) { length = 5; }
        var orders = [];
        for (var i = 0; i < length; i++) {
            var date = new Date((new Date()).getFullYear() - _years, 3, 1);
            orders.push({
                No: i,
                å—æ³¨æ—¥: sample.addDays(date, random(365 * _years)),
                å•†å“ã‚³ãƒ¼ãƒ‰: random(5) * 10 + random(5),
                é¡§å®¢ã‚³ãƒ¼ãƒ‰: random(_customerNames.length),
                æ•°é‡: random(20, 1),
                å€¤å¼•: random(3) == 0
        });
        }
        _log(orders, 'sample.ordersRaw()');
        return orders;
    }
    sample.ordersRaw = ordersRaw;
    function orders(length) {
        if (length === void 0) { length = 5; }
        var orders = [];
        _suspendLog();
        var ordersRaw = sample.ordersRaw(length);
        var products = sample.products();
        _resumeLog();
        for (var _i = 0, ordersRaw_1 = ordersRaw; _i < ordersRaw_1.length; _i++) {
            var order = ordersRaw_1[_i];
            orders.push({
                No: order.No,
                å•†å“å: products[order.å•†å“ã‚³ãƒ¼ãƒ‰].å•†å“å,
                å—æ³¨æ—¥: order.å—æ³¨æ—¥,
                é‡‘é¡: order.æ•°é‡ * products[order.å•†å“ã‚³ãƒ¼ãƒ‰].ä¾¡æ ¼ * (order.å€¤å¼• ? 8 : 10) / 10,
                å€¤å¼•: order.å€¤å¼•
        });
        }
        _log(orders, 'sample.orders()');
        return orders;
    }
    sample.orders = orders;
    function ordersDetail(length) {
        if (length === void 0) { length = 5; }
        var orders = [];
        _suspendLog();
        var ordersRaw = sample.ordersRaw(length);
        var categories = sample.categories();
        var products = sample.products();
        var customers = sample.customers();
        _resumeLog();
        for (var _i = 0, ordersRaw_2 = ordersRaw; _i < ordersRaw_2.length; _i++) {
            var order = ordersRaw_2[_i];
            orders.push({
                No: order.No,
                å—æ³¨æ—¥: order.å—æ³¨æ—¥,
                åˆ†é¡žå: categories[products[order.å•†å“ã‚³ãƒ¼ãƒ‰].åˆ†é¡žã‚³ãƒ¼ãƒ‰].åˆ†é¡žå,
                å•†å“å: products[order.å•†å“ã‚³ãƒ¼ãƒ‰].å•†å“å,
                é¡§å®¢å: customers[order.é¡§å®¢ã‚³ãƒ¼ãƒ‰].é¡§å®¢å,
                æ•°é‡: order.æ•°é‡,
            é‡‘é¡: order.æ•°é‡ * products[order.å•†å“ã‚³ãƒ¼ãƒ‰].ä¾¡æ ¼ * (order.å€¤å¼• ? 8 : 10) / 10,
                å€¤å¼•: order.å€¤å¼•
        });
        }
        _log(orders, 'sample.ordersDetail()');
        return orders;
    }
    sample.ordersDetail = ordersDetail;
    //================================================================
    // å£²ä¸Š
    //================================================================
    var Sales = /** @class */ (function () {
        function Sales() {
        }
        return Sales;
    }());
    sample.Sales = Sales;
    var SalesInYQM = /** @class */ (function () {
        function SalesInYQM() {
        }
        return SalesInYQM;
    }());
    sample.SalesInYQM = SalesInYQM;
    var SalesByCategory = /** @class */ (function () {
        function SalesByCategory() {
        }
        return SalesByCategory;
    }());
    sample.SalesByCategory = SalesByCategory;
    var SalesByCP = /** @class */ (function () {
        function SalesByCP() {
        }
        return SalesByCP;
    }());
    sample.SalesByCP = SalesByCP;
    var SalesArray = /** @class */ (function () {
        function SalesArray() {
        }
        return SalesArray;
    }());
    sample.SalesArray = SalesArray;
    function sales() {
        var sales = [];
        for (var i = 0; i < _years; i++) {
            sales.push({
                å¹´åº¦: _firstYear() + i + 'å¹´åº¦',
                é£²æ–™: random(150000, 50000) * _currencyRate,
                èª¿å‘³æ–™: random(150000, 50000) * _currencyRate,
                è“å­é¡ž: random(150000, 50000) * _currencyRate
        });
        }
        _log(sales, 'sample.sales()');
        return sales;
    }
    sample.sales = sales;
    function salesInYQM() {
        var sales = [];
        for (var i = 0; i < 3 * 12; i++) {
            sales.push({
                å¹´åº¦: _firstYear(3) + Math.floor(i / 12) + 'å¹´åº¦',
                å››åŠæœŸ: 'Q' + Math.floor((i % 12) / 3 + 1),
                æœˆ: (i + 3) % 12 + 1,
                å£²ä¸Š: random(15000, 5000) * _currencyRate
        });
        }
        _log(sales, 'sample.salesInYQM()');
        return sales;
    }
    sample.salesInYQM = salesInYQM;
    function salesByCategory() {
        var sales = [];
        for (var i = 0; i < 5; i++) {
            sales.push({
                åˆ†é¡žå: _categoryNames[i],
                å£²ä¸Š: random(150000, 50000) * _currencyRate
        });
        }
        _log(sales, 'sample.salesByCategory()');
        return sales;
    }
    sample.salesByCategory = salesByCategory;
    function salesByCP() {
        var sales = [];
        _suspendLog();
        var products = sample.products();
        var categories = sample.categories();
        _resumeLog();
        var current = random(15000, 5000);
        for (var _i = 0, products_1 = products; _i < products_1.length; _i++) {
            var product = products_1[_i];
            if (product.åˆ†é¡žã‚³ãƒ¼ãƒ‰ == 4)
            break;
            sales.push({
                åˆ†é¡žå: categories[product.åˆ†é¡žã‚³ãƒ¼ãƒ‰].åˆ†é¡žå,
                å•†å“å: product.å•†å“å,
                å£²ä¸Š: Math.floor(current *= 0.95) * _currencyRate
        });
        }
        _log(sales, 'sample.salesByCP()');
        return sales;
    }
    sample.salesByCP = salesByCP;
    function salesArray() {
        var sales = [];
        for (var i = 0; i < _years; i++) {
            sales.push({
                å¹´åº¦: _firstYear() + i + 'å¹´åº¦',
                å£²ä¸Š: []
        });
            for (var j = 0; j < _categoryNames.length; j++) {
                sales[i].å£²ä¸Š.push(random(15000, 5000) * _currencyRate);
            }
        }
        _log(sales, 'sample.salesArray()');
        return sales;
    }
    sample.salesArray = salesArray;
    //================================================================
    // ãƒãƒ£ãƒ¼ãƒˆ
    //================================================================
    var Stock = /** @class */ (function () {
        function Stock() {
        }
        return Stock;
    }());
    sample.Stock = Stock;
    function stocks(length) {
        if (length === void 0) { length = 100; }
        var stocks = [];
        var open = random(600, 400), high, low;
        var date = sample.addDays(new Date(), -length);
        for (var i = 0; i < length; i++) {
            stocks.push({
                date: sample.addDays(date, i),
                open: open,
                high: high = open + random(20),
                low: low = open - random(20),
                close: open = random(high, low),
                volume: random(15000) * _currencyRate
            });
        }
        _log(stocks, 'sample.stocks()');
        return stocks;
    }
    sample.stocks = stocks;
    var ProfitLoss = /** @class */ (function () {
        function ProfitLoss() {
        }
        return ProfitLoss;
    }());
    sample.ProfitLoss = ProfitLoss;
    function profitLoss() {
        var pl = [
            { é …ç›®å: 'å£²ä¸Šé«˜', æç›Š: random(110000, 90000) * _currencyRate },
        { é …ç›®å: 'å£²ä¸ŠåŽŸä¾¡', æç›Š: -random(60000, 50000) * _currencyRate },
        { é …ç›®å: 'è²©å£²ç®¡ç†è²»', æç›Š: -random(40000, 30000) * _currencyRate },
        { é …ç›®å: 'å–¶æ¥­å¤–åŽç›Š', æç›Š: random(600, 500) * _currencyRate },
        { é …ç›®å: 'å–¶æ¥­å¤–è²»ç”¨', æç›Š: -random(5000, 400) * _currencyRate }
    ];
        _log(pl, 'sample.profitLoss()');
        return pl;
    }
    sample.profitLoss = profitLoss;
    var SalesItems = /** @class */ (function () {
        function SalesItems() {
        }
        return SalesItems;
    }());
    sample.SalesItems = SalesItems;
    function salesItems() {
        var items = [
            { æ®µéšŽ: 'æ½œåœ¨é¡§å®¢', æ¡ˆä»¶æ•°: 200 },
        { æ®µéšŽ: 'è¦‹è¾¼ã¿ã‚ã‚Š', æ¡ˆä»¶æ•°: 100 },
        { æ®µéšŽ: 'è§£æ±º', æ¡ˆä»¶æ•°: 50 },
        { æ®µéšŽ: 'ææ¡ˆ', æ¡ˆä»¶æ•°: 20 },
        { æ®µéšŽ: 'æœ€çµ‚', æ¡ˆä»¶æ•°: 10 },
    ];
        _log(items, 'sample.salesItems()');
        return items;
    }
    sample.salesItems = salesItems;
    var PolarPoint = /** @class */ (function () {
        function PolarPoint() {
        }
        return PolarPoint;
    }());
    sample.PolarPoint = PolarPoint;
    function polars(step) {
        if (step === void 0) { step = 10; }
        var polars = [];
        for (var p = 0; p < 360; p += step) {
            polars.push({
                è§’åº¦: p,
                è¨ˆæ¸¬å€¤1: random(100),
                è¨ˆæ¸¬å€¤2: random(100)
        });
        }
        _log(polars, 'sample.polars()');
        return polars;
    }
    sample.polars = polars;
    //================================================================
    // åˆ†é¡ž
    //================================================================
    var _categoryNames = ['é£²æ–™', 'èª¿å‘³æ–™', 'è“å­é¡ž', 'ä¹³è£½å“', 'ç©€é¡žã€ã‚·ãƒªã‚¢ãƒ«', 'è‚‰é¡ž', 'åŠ å·¥é£Ÿå“', 'é­šä»‹é¡ž'];
    var _categoryImages = ['drink.png', 'condiment.png', 'sweet.png', 'dairy.png', 'grain.png', 'meat.png', 'produce.png', 'seafood.png'];
    var Category = /** @class */ (function () {
        function Category() {
        }
        return Category;
    }());
    sample.Category = Category;
    function categoryNames() {
        _log(_categoryNames, 'sample.categoryNames()');
        return _categoryNames;
    }
    sample.categoryNames = categoryNames;
    function categories() {
        var categories = [];
        for (var i = 0; i < _categoryNames.length; i++) {
            categories.push({
                åˆ†é¡žã‚³ãƒ¼ãƒ‰: i,
                åˆ†é¡žå: _categoryNames[i],
                ç”»åƒ: sample.path + 'images/' + _categoryImages[i]
        });
        }
        _log(categories, 'sample.categories()');
        return categories;
    }
    sample.categories = categories;
    //================================================================
    // å•†å“
    //================================================================
    var _productNames = [
        ['æžœæ±100%ã‚ªãƒ¬ãƒ³ã‚¸', 'æžœæ±100%ã‚°ãƒ¬ãƒ¼ãƒ—', 'æžœæ±100%ãƒ¬ãƒ¢ãƒ³', 'æžœæ±100%ãƒ”ãƒ¼ãƒ', 'ã‚³ãƒ¼ãƒ’ãƒ¼ãƒžã‚¤ãƒ«ãƒ‰', 'ã‚³ãƒ¼ãƒ’ãƒ¼ãƒ“ã‚¿ãƒ¼', 'ã‚³ãƒ¼ãƒ’ãƒ¼ãƒŸãƒ«ã‚¯', 'ãƒ”ãƒªãƒ”ãƒªãƒ“ãƒ¼ãƒ«', 'ã‚ªã‚¿ãƒ«ç™½ãƒ©ãƒ™ãƒ«', 'ãƒãƒ¼ãƒ‰ãƒ¯ã‚¤ãƒ³'],
        ['ãƒ›ãƒ¯ã‚¤ãƒˆã‚½ãƒ«ãƒˆ', 'ãƒ–ãƒ©ãƒƒã‚¯ãƒšãƒƒãƒ‘ãƒ¼', 'ãƒ”ãƒ¥ã‚¢ã‚·ãƒ¥ã‚¬ãƒ¼', 'ã†ã¾ã„ç´ ', 'ãƒ”ãƒ¥ã‚¢ãƒ‡ãƒŸã‚°ãƒ©ã‚¹ã‚½ãƒ¼ã‚¹', 'ã ã—ã‹ã¤ãŠ', 'ã ã—ã“ã‚“ã¶', 'ãƒ”ãƒªã‚«ãƒ©ã‚¿ãƒã‚¹ã‚³', 'ã®ã‚Šå±±æ¤’', 'ç‰¹è£½å’Œé¢¨é†¤æ²¹'],
        ['ãƒãƒ‹ãƒ©ã‚¯ãƒªãƒ¼ãƒ ã‚¢ã‚¤ã‚¹', 'ãƒãƒ§ã‚³ã‚¯ãƒªãƒ¼ãƒ ã‚¢ã‚¤ã‚¹', 'ç´…èŒ¶ãƒãƒ¼', 'ã˜ã‚ƒãŒãƒãƒƒãƒ—ã‚¹', 'ã‚¢ãƒ¡ãƒªã‚«ãƒ³ã‚¯ãƒ©ãƒƒã‚«ãƒ¼', 'ãƒãƒŠãƒŠã‚­ãƒ£ãƒ³ãƒ‡ã‚£ãƒ¼', 'ãƒ¡ãƒ­ãƒ³ãƒŸãƒ«ã‚¯ã‚­ãƒ£ãƒ³ãƒ‡ã‚£ãƒ¼', 'å°å€‰ã‚ã‚“ã±ã‚“', 'ã‚¤ãƒ³ãƒ‰ã‚«ãƒ¬ãƒ¼ãƒ‘ãƒ³', 'ãƒãƒ¼ã‚ºã‚ã‚“ã±ã‚“'],
        ['ãƒ­ãƒƒã‚­ãƒ¼ãƒãƒ¼ã‚º', 'ãƒ‘ãƒ«ãƒ¡ã‚¶ãƒ³ãƒãƒ¼ã‚º', 'ãƒ•ãƒ¬ãƒƒã‚·ãƒ¥ãƒã‚¿ãƒ¼', 'ãƒ©ã‚¤ãƒ•ãƒžãƒ¼ã‚¬ãƒªãƒ³', 'ãƒ­ãƒ¼ã‚«ãƒ­ãƒªãƒ¼ç‰›ä¹³', 'ç‰›ä¹³ãƒžã‚¤ãƒ«ãƒ‰', 'ã‚¹ãƒˆãƒ­ãƒ™ãƒªãƒ¼ãƒ¨ãƒ¼ã‚°ãƒ«ãƒˆ', 'ãƒ–ãƒ«ãƒ¼ãƒ™ãƒªãƒ¼ãƒ¨ãƒ¼ã‚°ãƒ«ãƒˆ', 'ãƒ©ã‚ºãƒ™ãƒªãƒ¼ãƒ¨ãƒ¼ã‚°ãƒ«ãƒˆ', 'ã‚³ã‚³ãƒŠãƒƒãƒ„ãƒŸãƒ«ã‚¯'],
        ['ãƒ¢ãƒ¼ãƒ‹ãƒ³ã‚°ãƒ–ãƒ¬ãƒƒãƒ‰', 'ãƒã‚¿ãƒ¼ãƒˆãƒ¼ã‚¹ãƒˆ', 'ãƒã‚±ãƒƒãƒˆãƒ•ãƒ©ãƒ³ã‚¹', 'æ¥µä¸Šãƒ‘ã‚¹ã‚¿', 'æ¥µä¸Šãƒžã‚«ãƒ­ãƒ‹', 'ä¼çµ±ã‚¹ãƒ‘ã‚²ãƒƒãƒ†ã‚£', 'ãƒ˜ãƒ«ã‚·ãƒ¼ã‚¯ãƒ©ãƒƒã‚«ãƒ¼', 'ã‚³ãƒ¼ãƒ³ãƒ•ãƒ¬ãƒ¼ã‚¯ãƒ—ãƒ¬ãƒ¼ãƒ³', 'ã‚³ãƒ¼ãƒ³ãƒ•ãƒ¬ãƒ¼ã‚¯ãƒãƒ§ã‚³', 'ã‚³ãƒ¼ãƒ³ãƒ•ãƒ¬ãƒ¼ã‚¯ã‚·ãƒ¥ã‚¬ãƒ¼'],
        ['ã‚¢ãƒ¡ãƒªã‚«ãƒ³ãƒãƒ¼ã‚¯', 'ã†ã™å‘³ã‚¦ã‚¤ãƒ³ãƒŠãƒ¼', 'ãƒ™ã‚¿ãƒ¼ãƒ­ãƒ¼ã‚¹ãƒˆãƒãƒ ', 'ãƒ™ã‚¿ãƒ¼ãƒ—ãƒ¬ã‚¹ãƒãƒ ', 'ãƒ™ã‚¿ãƒ¼ç”Ÿãƒãƒ ', 'ç‰¹è£½ã‚µãƒ©ãƒŸ', 'å’Œé¢¨ãƒãƒ³ãƒãƒ¼ã‚°ãƒ¬ãƒˆãƒ«ãƒˆ', 'ç…§ç„¼ããƒŸãƒ¼ãƒˆãƒœãƒ¼ãƒ«', 'ãƒŸãƒƒã‚¯ã‚¹ãƒãƒ ', 'ãƒŸãƒ¼ãƒˆãƒãƒ¼'],
        ['ã‚‚ã‚ã‚“ã©ã†ãµç‰¹ä¸Š', 'ãã¬ã”ã—ã©ã†ãµç‰¹ä¸Š', 'å†·å‡ãƒŸãƒƒã‚¯ã‚¹ãƒ™ã‚¸ã‚¿ãƒ–ãƒ«', 'å†·å‡ã‚¯ãƒªãƒ¼ãƒ ã‚³ãƒ­ãƒƒã‚±', 'å†·å‡ã‚³ãƒ¼ãƒ³ã‚¯ãƒªãƒ¼ãƒ ã‚³ãƒ­ãƒƒã‚±', 'å†·å‡ãƒãƒ†ãƒˆã‚³ãƒ­ãƒƒã‚±', 'å†·å‡æžè±†', 'å†·å‡ã‚„ããŠã«ãŽã‚Š', 'ä¹¾ç‡¥ãƒãƒŠãƒŠ', 'ä¹¾ç‡¥ã‚¢ãƒƒãƒ—ãƒ«'],
        ['ç‰¹é¸å‘³ã®ã‚Š', 'åŒ—æµ·é“æ˜†å¸ƒ', 'ã‚„ãã„ã‹ã™ã‚‹ã‚ãã‚“', 'é£Ÿå“ã‚ã‹ã‚', 'ãµã‚Šã‹ã‘ã‹ã¤ãŠé¢¨å‘³', 'ãµã‚Šã‹ã‘é®­é¢¨å‘³', 'å¤§é™¸ã‚µãƒ¼ãƒ¢ãƒ³', 'ç‰¹é¸ã«ã¼ã—', 'æœ¬ãŒã¤ãŠç‰¹ä¸Š', 'ã“ã‚ã‚‚ã¯ã‚“ãºã‚“']
    ];
    var Product = /** @class */ (function () {
        function Product() {
        }
        return Product;
    }());
    sample.Product = Product;
    var ProductTreeNode = /** @class */ (function () {
        function ProductTreeNode() {
        }
        return ProductTreeNode;
    }());
    sample.ProductTreeNode = ProductTreeNode;
    var _productNamesCache = null;
    function productNames() {
        if (_productNamesCache == null) {
            _productNamesCache = [];
            for (var _i = 0, _productNames_1 = _productNames; _i < _productNames_1.length; _i++) {
                var productNames_1 = _productNames_1[_i];
                for (var _a = 0, productNames_2 = productNames_1; _a < productNames_2.length; _a++) {
                    var productName = productNames_2[_a];
                    _productNamesCache.push(productName);
                }
            }
        }
        _log(_productNamesCache, 'sample.productNames()');
        return _productNamesCache;
    }
    sample.productNames = productNames;
    var _productsCache = null;
    function products() {
        if (_productsCache == null) {
            _productsCache = [];
            var id = 0;
            for (var i = 0; i < _productNames.length; i++) {
                for (var j = 0; j < _productNames[i].length; j++) {
                    _productsCache.push({
                        å•†å“ã‚³ãƒ¼ãƒ‰: id++,
                        å•†å“å: _productNames[i][j],
                        ä¾¡æ ¼: random(40, 10) * _currencyRate,
                        åˆ†é¡žã‚³ãƒ¼ãƒ‰: i
                });
                }
            }
        }
        _log(_productsCache, 'sample.products()');
        return _productsCache;
    }
    sample.products = products;
    function productsTree() {
        var productsTree = [];
        _suspendLog();
        var categories = sample.categories();
        _resumeLog();
        for (var i = 0; i < _categoryNames.length; i++) {
            productsTree.push({
                åå‰: categories[i].åˆ†é¡žå,
                ç”»åƒ: categories[i].ç”»åƒ,
                items: []
        });
            for (var j = 0; j < _productNames[i].length; j++) {
                productsTree[i].items.push({
                    åå‰: _productNames[i][j]
            });
            }
        }
        _log(productsTree, 'sample.productsTree()');
        return productsTree;
    }
    sample.productsTree = productsTree;
    //================================================================
    // é¡§å®¢
    //================================================================
    var _customerNames = ['å°æ–™ç†ãªã‚“ã”ã', 'å‰²çƒ¹ãµã˜ã„', 'æµ·é®®æ–™ç†ãã˜ã‚‰', 'å±…é…’å±‹ãªãªã¹ãˆ', 'é…’è”µã§ã‚“'];
    var Customer = /** @class */ (function () {
        function Customer() {
        }
        return Customer;
    }());
    sample.Customer = Customer;
    function customerNames() {
        _log(_customerNames, 'sample.customerNames()');
        return _customerNames;
    }
    sample.customerNames = customerNames;
    var _customersCache = null;
    function customers() {
        if (_customersCache == null) {
            _customersCache = [];
            for (var i = 0; i < _customerNames.length; i++) {
                _customersCache.push({
                    é¡§å®¢ã‚³ãƒ¼ãƒ‰: i,
                    é¡§å®¢å: _customerNames[i]
            });
            }
        }
        _log(_customersCache, 'sample.customers()');
        return _customersCache;
    }
    sample.customers = customers;
    //================================================================
    // å›½
    //================================================================
    var _countryNames = ['ã‚¢ãƒ¡ãƒªã‚«', 'æ—¥æœ¬', 'ä¸­å›½', 'ãƒ‰ã‚¤ãƒ„', 'ã‚¤ã‚®ãƒªã‚¹', 'ã‚¢ãƒ•ã‚¬ãƒ‹ã‚¹ã‚¿ãƒ³', 'ã‚¢ãƒ«ãƒãƒ‹ã‚¢', 'ã‚¢ãƒ«ã‚¸ã‚§ãƒªã‚¢', 'ã‚¢ãƒ¡ãƒªã‚«é ˜ã‚µãƒ¢ã‚¢', 'ã‚¢ãƒ³ãƒ‰ãƒ©', 'ã‚¢ãƒ³ã‚´ãƒ©', 'ã‚¢ãƒ³ã‚°ã‚£ãƒ©', 'ã‚¢ãƒ³ãƒ†ã‚£ã‚°ã‚¢', 'ã‚¢ãƒ«ã‚¼ãƒ³ãƒãƒ³', 'ã‚¢ãƒ«ãƒ¡ãƒ‹ã‚¢', 'ã‚¢ãƒ«ãƒ', 'ã‚ªãƒ¼ã‚¹ãƒˆãƒ©ãƒªã‚¢', 'ã‚ªãƒ¼ã‚¹ãƒˆãƒªã‚¢', 'ã‚¢ã‚¼ãƒ«ãƒã‚¤ã‚¸ãƒ£ãƒ³', 'ãƒãƒãƒž', 'ãƒãƒ¼ãƒ¬ãƒ¼ãƒ³', 'ãƒãƒ³ã‚°ãƒ©ãƒ‡ã‚·ãƒ¥', 'ãƒãƒ«ãƒãƒ‰ã‚¹', 'ãƒ™ãƒ©ãƒ«ãƒ¼ã‚·', 'ãƒ™ãƒ«ã‚®ãƒ¼', 'ãƒ™ãƒªãƒ¼ã‚º', 'ãƒ™ãƒ‹ãƒ³', 'ãƒãƒŸãƒ¥ãƒ¼ãƒ€', 'ãƒ–ãƒ¼ã‚¿ãƒ³', 'ãƒœãƒªãƒ“ã‚¢', 'ãƒœãƒãƒ¼ãƒ«å³¶', 'ãƒœã‚¹ãƒ‹ã‚¢', 'ãƒœãƒ„ãƒ¯ãƒŠ', 'ãƒ–ãƒ©ã‚¸ãƒ«', 'ãƒ–ãƒ«ãƒã‚¤', 'ãƒ–ãƒ«ã‚¬ãƒªã‚¢', 'ãƒ–ãƒ«ã‚­ãƒŠãƒ•ã‚¡ã‚½', 'ãƒ–ãƒ«ãƒ³ã‚¸', 'ã‚«ãƒ³ãƒœã‚¸ã‚¢', 'ã‚«ãƒ¡ãƒ«ãƒ¼ãƒ³', 'ã‚«ãƒŠãƒ€', 'ã‚«ãƒŠãƒªã‚¢è«¸å³¶', 'ã‚«ãƒ¼ãƒœãƒ™ãƒ«ãƒ‡', 'ã‚±ã‚¤ãƒžãƒ³è«¸å³¶', 'ä¸­å¤®ã‚¢ãƒ•ãƒªã‚«å…±å’Œå›½', 'ãƒãƒ£ãƒ‰', 'ãƒãƒ£ãƒãƒ«è«¸å³¶', 'ãƒãƒª', 'ã‚¯ãƒªã‚¹ãƒžã‚¹å³¶', 'ã‚³ã‚³ã‚¹å³¶', 'ã‚³ãƒ­ãƒ³ãƒ“ã‚¢', 'ã‚³ãƒ¢ãƒ­', 'ã‚³ãƒ³ã‚´', 'ã‚¯ãƒƒã‚¯è«¸å³¶', 'ã‚³ã‚¹ã‚¿ãƒªã‚«', 'ã‚³ãƒ¼ãƒˆã‚¸ãƒœãƒ¯ãƒ¼ãƒ«', 'ã‚¯ãƒ­ã‚¢ãƒã‚¢', 'ã‚­ãƒ¥ãƒ¼ãƒ', 'ã‚­ãƒ¥ãƒ©ã‚½ãƒ¼', 'ã‚­ãƒ—ãƒ­ã‚¹', 'ãƒã‚§ã‚³å…±å’Œå›½', 'ãƒ‡ãƒ³ãƒžãƒ¼ã‚¯', 'ã‚¸ãƒ–ãƒ', 'ãƒ‰ãƒŸãƒ‹ã‚«', 'ãƒ‰ãƒŸãƒ‹ã‚«å…±å’Œå›½', 'æ±ãƒãƒ¢ãƒ¼ãƒ«', 'ã‚¨ã‚¯ã‚¢ãƒ‰ãƒ«', 'ã‚¨ã‚¸ãƒ—ãƒˆ', 'ã‚¨ãƒ«ã‚µãƒ«ãƒãƒ‰ãƒ«', 'èµ¤é“ã‚®ãƒ‹ã‚¢', 'ã‚¨ãƒªãƒˆãƒªã‚¢', 'ã‚¨ã‚¹ãƒˆãƒ‹ã‚¢', 'ã‚¨ãƒã‚ªãƒ”ã‚¢', 'ãƒ•ã‚©ãƒ¼ã‚¯ãƒ©ãƒ³ãƒ‰è«¸å³¶', 'ãƒ•ã‚§ãƒ­ãƒ¼è«¸å³¶', 'ãƒ•ã‚£ã‚¸ãƒ¼', 'ãƒ•ã‚£ãƒ³ãƒ©ãƒ³ãƒ‰', 'ãƒ•ãƒ©ãƒ³ã‚¹', 'ä»é ˜ã‚®ã‚¢ãƒŠ', 'ãƒ•ãƒ©ãƒ³ã‚¹é ˜ãƒãƒªãƒã‚·ã‚¢', 'ãƒ•ãƒ©ãƒ³ã‚¹å—éƒ¨ãƒ†ãƒ«', 'ã‚¬ãƒœãƒ³', 'ã‚¬ãƒ³ãƒ“ã‚¢', 'ã‚¸ãƒ§ãƒ¼ã‚¸ã‚¢', 'ã‚¬ãƒ¼ãƒŠ', 'ã‚¸ãƒ–ãƒ©ãƒ«ã‚¿ãƒ«', 'ã‚®ãƒªã‚·ãƒ£', 'ã‚°ãƒªãƒ¼ãƒ³ãƒ©ãƒ³ãƒ‰', 'ã‚°ãƒ¬ãƒŠãƒ€', 'ã‚°ã‚¢ãƒ‰ãƒ«ãƒ¼ãƒ—å³¶', 'ã‚°ã‚¢ãƒ ', 'ã‚°ã‚¢ãƒ†ãƒžãƒ©', 'ã‚®ãƒ‹ã‚¢', 'ã‚¬ã‚¤ã‚¢ãƒŠ', 'ãƒã‚¤ãƒ', 'ãƒ›ãƒ³ã‚¸ãƒ¥ãƒ©ã‚¹', 'é¦™æ¸¯', 'ãƒãƒ³ã‚¬ãƒªãƒ¼', 'ã‚¢ã‚¤ã‚¹ãƒ©ãƒ³ãƒ‰', 'ã‚¤ãƒ³ãƒ‰', 'ã‚¤ãƒ³ãƒ‰ãƒã‚·ã‚¢', 'ã‚¤ãƒ©ãƒ³', 'ã‚¤ãƒ©ã‚¯', 'ã‚¢ã‚¤ãƒ«ãƒ©ãƒ³ãƒ‰', 'ãƒžãƒ³å³¶', 'ã‚¤ã‚¹ãƒ©ã‚¨ãƒ«', 'ã‚¤ã‚¿ãƒªã‚¢', 'ã‚¸ãƒ£ãƒžã‚¤ã‚«', 'ãƒ¨ãƒ«ãƒ€ãƒ³', 'ã‚«ã‚¶ãƒ•ã‚¹ã‚¿ãƒ³', 'ã‚±ãƒ‹ã‚¢', 'ã‚­ãƒªãƒã‚¹', 'åŒ—æœé®®', 'éŸ“å›½', 'ã‚¯ã‚¦ã‚§ãƒ¼ãƒˆ', 'ã‚­ãƒ«ã‚®ã‚¹ã‚¿ãƒ³', 'ãƒ©ã‚ªã‚¹', 'ãƒ©ãƒˆãƒ“ã‚¢', 'ãƒ¬ãƒãƒŽãƒ³', 'ãƒ¬ã‚½ãƒˆ', 'ãƒªãƒ™ãƒªã‚¢', 'ãƒªãƒ“ã‚¢', 'ãƒªãƒ’ãƒ†ãƒ³ã‚·ãƒ¥ã‚¿ã‚¤ãƒ³', 'ãƒªãƒˆã‚¢ãƒ‹ã‚¢', 'ãƒ«ã‚¯ã‚»ãƒ³ãƒ–ãƒ«ã‚¯', 'ãƒžã‚«ã‚ª', 'ãƒžã‚±ãƒ‰ãƒ‹ã‚¢', 'ãƒžãƒ€ã‚¬ã‚¹ã‚«ãƒ«', 'ãƒžãƒ¬ãƒ¼ã‚·ã‚¢', 'ãƒžãƒ©ã‚¦ã‚¤', 'ãƒ¢ãƒ«ãƒ‡ã‚£ãƒ–', 'ãƒžãƒª', 'ãƒžãƒ«ã‚¿', 'ãƒžãƒ¼ã‚·ãƒ£ãƒ«è«¸å³¶', 'ãƒžãƒ«ãƒ†ã‚£ãƒ‹ãƒ¼ã‚¯å³¶', 'ãƒ¢ãƒ¼ãƒªã‚¿ãƒ‹ã‚¢', 'ãƒ¢ãƒ¼ãƒªã‚·ãƒ£ã‚¹', 'ãƒžãƒ¨ãƒƒãƒˆå³¶', 'ãƒ¡ã‚­ã‚·ã‚³', 'ãƒŸãƒƒãƒ‰ã‚¦ã‚§ãƒ¼è«¸å³¶', 'ãƒ¢ãƒ«ãƒ‰ãƒ', 'ãƒ¢ãƒŠã‚³', 'ãƒ¢ãƒ³ã‚´ãƒ«', 'ãƒ¢ãƒ³ãƒˆã‚»ãƒ©ãƒˆ', 'ãƒ¢ãƒ­ãƒƒã‚³', 'ãƒ¢ã‚¶ãƒ³ãƒ“ãƒ¼ã‚¯', 'ãƒŸãƒ£ãƒ³ãƒžãƒ¼', 'ãƒŠãƒ³ãƒ“ã‚¢', 'ãƒŠã‚¦ãƒ«', 'ãƒãƒ‘ãƒ¼ãƒ«', 'ã‚ªãƒ©ãƒ³ãƒ€é ˜ã‚¢ãƒ³ãƒ†ã‚£ãƒ«', 'ã‚ªãƒ©ãƒ³ãƒ€', 'ãƒãƒ“ã‚¹å³¶', 'ãƒ‹ãƒ¥ãƒ¼ã‚«ãƒ¬ãƒ‰ãƒ‹ã‚¢', 'ãƒ‹ãƒ¥ãƒ¼ã‚¸ãƒ¼ãƒ©ãƒ³ãƒ‰', 'ãƒ‹ã‚«ãƒ©ã‚°ã‚¢', 'ãƒ‹ã‚¸ã‚§ãƒ¼ãƒ«', 'ãƒŠã‚¤ã‚¸ã‚§ãƒªã‚¢', 'ãƒ‹ã‚¦ã‚¨', 'ãƒŽãƒ¼ãƒ•ã‚©ãƒ¼ã‚¯å³¶', 'ãƒŽãƒ«ã‚¦ã‚§ãƒ¼', 'ã‚ªãƒžãƒ¼ãƒ³', 'ãƒ‘ã‚­ã‚¹ã‚¿ãƒ³', 'ãƒ‘ãƒ©ã‚ªå³¶', 'ãƒ‘ãƒ¬ã‚¹ãƒãƒŠ', 'ãƒ‘ãƒŠãƒž', 'ãƒ‘ãƒ—ã‚¢ãƒ‹ãƒ¥ãƒ¼ã‚®ãƒ‹ã‚¢', 'ãƒ‘ãƒ©ã‚°ã‚¢ã‚¤', 'ãƒšãƒ«ãƒ¼', 'ãƒ•ã‚£ãƒªãƒ”ãƒ³', 'ãƒ”ãƒˆã‚±ã‚¢ãƒ³å³¶', 'ãƒãƒ¼ãƒ©ãƒ³ãƒ‰', 'ãƒãƒ«ãƒˆã‚¬ãƒ«', 'ãƒ—ã‚¨ãƒ«ãƒˆãƒªã‚³', 'ã‚«ã‚¿ãƒ¼ãƒ«', 'ãƒ¢ãƒ³ãƒ†ãƒã‚°ãƒ­å…±å’Œå›½', 'ã‚»ãƒ«ãƒ“ã‚¢å…±å’Œå›½', 'ãƒ¬ãƒ¦ãƒ‹ã‚ªãƒ³', 'ãƒ«ãƒ¼ãƒžãƒ‹ã‚¢', 'ãƒ­ã‚·ã‚¢', 'ãƒ«ãƒ¯ãƒ³ãƒ€', 'ã‚µãƒ³ãƒãƒ«ãƒ†ãƒ«ãƒŸ', 'ã‚»ãƒ³ãƒˆãƒ¦ãƒ¼ã‚¹ãƒ†ãƒ¼ã‚·ãƒ£ã‚¹', 'ã‚»ãƒ³ãƒˆãƒ˜ãƒ¬ãƒŠ', 'ã‚»ãƒ³ãƒˆã‚­ãƒƒãƒ„ãƒ»ãƒã‚¤ãƒ“ã‚¹', 'ã‚»ãƒ³ãƒˆãƒ«ã‚·ã‚¢', 'ã‚µãƒ³ãƒ»ãƒžãƒ«ã‚¿ãƒ³å³¶', 'ã‚µã‚¤ãƒ‘ãƒ³å³¶', 'ã‚µãƒ¢ã‚¢', 'ã‚µãƒ³ãƒ»ãƒžãƒªãƒŽ', 'ã‚µã‚¦ã‚¸ã‚¢ãƒ©ãƒ“ã‚¢', 'ã‚¹ã‚³ãƒƒãƒˆãƒ©ãƒ³ãƒ‰', 'ã‚»ãƒã‚¬ãƒ«', 'ã‚»ãƒ«ãƒ“ã‚¢', 'ã‚»ã‚¤ã‚·ã‚§ãƒ«', 'ã‚·ã‚¨ãƒ©ãƒ¬ã‚ªãƒ', 'ã‚·ãƒ³ã‚¬ãƒãƒ¼ãƒ«', 'ã‚¹ãƒ­ãƒã‚­ã‚¢', 'ã‚¹ãƒ­ãƒ™ãƒ‹ã‚¢', 'ã‚½ãƒ­ãƒ¢ãƒ³è«¸å³¶', 'ã‚½ãƒžãƒªã‚¢', 'å—ã‚¢ãƒ•ãƒªã‚«', 'ã‚¹ãƒšã‚¤ãƒ³', 'ã‚¹ãƒªãƒ©ãƒ³ã‚«', 'ã‚¹ãƒ¼ãƒ€ãƒ³', 'ã‚¹ãƒªãƒŠãƒ ', 'ã‚¹ãƒ¯ã‚¸ãƒ©ãƒ³ãƒ‰', 'ã‚¹ã‚¦ã‚§ãƒ¼ãƒ‡ãƒ³', 'ã‚¹ã‚¤ã‚¹', 'ã‚·ãƒªã‚¢', 'ã‚¿ãƒ’ãƒå³¶', 'å°æ¹¾', 'ã‚¿ã‚¸ã‚­ã‚¹ã‚¿ãƒ³', 'ã‚¿ãƒ³ã‚¶ãƒ‹ã‚¢', 'ã‚¿ã‚¤', 'ãƒˆãƒ¼ã‚´', 'ãƒˆã‚±ãƒ©ã‚¦è«¸å³¶', 'ãƒˆãƒ³ã‚¬', 'ãƒˆãƒªãƒ‹ãƒ€ãƒ¼ãƒ‰ãƒ»ãƒˆãƒã‚´', 'ãƒãƒ¥ãƒ‹ã‚¸ã‚¢', 'ãƒˆãƒ«ã‚³', 'ãƒˆãƒ«ã‚¯ãƒ¡ãƒ‹ã‚¹ã‚¿ãƒ³', 'ã‚¿ãƒ¼ã‚¯ã‚¹ãƒ»ã‚«ã‚¤ã‚³ã‚¹è«¸å³¶', 'ãƒ„ãƒãƒ«', 'ã‚¦ã‚¬ãƒ³ãƒ€', 'ã‚¦ã‚¯ãƒ©ã‚¤ãƒŠ', 'ã‚¢ãƒ©ãƒ–é¦–é•·å›½é€£é‚¦', 'ã‚¤ã‚®ãƒªã‚¹', 'ã‚¦ãƒ«ã‚°ã‚¢ã‚¤', 'ã‚¦ã‚ºãƒ™ã‚­ã‚¹ã‚¿ãƒ³', 'ãƒãƒŒã‚¢ãƒ„', 'ãƒãƒã‚«ãƒ³å¸‚å›½', 'ãƒ™ãƒã‚ºã‚¨ãƒ©', 'ãƒ™ãƒˆãƒŠãƒ ', 'ãƒãƒ¼ã‚¸ãƒ³è«¸å³¶ï¼ˆã‚¤ã‚®ãƒªã‚¹ï¼‰', 'ãƒãƒ¼ã‚¸ãƒ³è«¸å³¶ï¼ˆç±³å›½ï¼‰', 'ã‚¦ã‚§ãƒ¼ã‚¯å³¶', 'ã‚¤ã‚¨ãƒ¡ãƒ³', 'ã‚¶ã‚¤ãƒ¼ãƒ«', 'ã‚¶ãƒ³ãƒ“ã‚¢', 'ã‚¸ãƒ³ãƒãƒ–ã‚¨'];
    var Country = /** @class */ (function () {
        function Country() {
        }
        return Country;
    }());
    sample.Country = Country;
    function countryNames() {
        _log(_countryNames, 'sample.countryNames()');
        return _countryNames;
    }
    sample.countryNames = countryNames;
    function countries(length) {
        if (length === void 0) { length = _countryNames.length; }
        var countries = [];
        for (var i = 0; i < length; i++) {
            countries.push({
                å›½ã‚³ãƒ¼ãƒ‰: i,
                å›½å: _countryNames[i]
        });
        }
        _log(countries, 'sample.countries()');
        return countries;
    }
    sample.countries = countries;
    //================================================================
    // ãƒ¦ãƒ¼ãƒ†ã‚£ãƒªãƒ†ã‚£
    //================================================================
    function random(max, min) {
        if (min === void 0) { min = 0; }
        return Math.floor(Math.random() * (max - min)) + min;
    }
    sample.random = random;
    function randomArray(items) {
        return items[random(items.length)];
    }
    sample.randomArray = randomArray;
    function addDays(value, days) {
        return new Date(value.getFullYear(), value.getMonth(), value.getDate() + days);
    }
    sample.addDays = addDays;
    function addMonths(value, months) {
        return new Date(value.getFullYear(), value.getMonth() + months, value.getDate());
    }
    sample.addMonths = addMonths;
    var KeyValue = /** @class */ (function () {
        function KeyValue() {
        }
        return KeyValue;
    }());
    function keyValues(names, values) {
        var keyValues = [];
        for (var i = 0; i < names.length; i++) {
            if (names[i]) {
                keyValues.push({
                    name: names[i],
                    value: values ? values[i] : i
                });
            }
        }
        return keyValues;
    }
    sample.keyValues = keyValues;
    function enumToKeyValues(enumObj, names) {
        var keyValues = [];
        for (var key in enumObj) {
            var i = Number(key);
            if (!isNaN(i)) {
                if (!names) {
                    keyValues.push({
                        name: enumObj[i],
                        value: i
                    });
                }
                else if (names[i]) {
                    keyValues.push({
                        name: names && names[i],
                        value: i
                    });
                }
            }
        }
        return keyValues;
    }
    sample.enumToKeyValues = enumToKeyValues;
    function stringsToKeyValues(names) {
        var keyValues = [];
        for (var i = 0; i < names.length; i++) {
            if (names[i]) {
                keyValues.push({
                    name: names[i],
                    value: i
                });
            }
        }
        return keyValues;
    }
    sample.stringsToKeyValues = stringsToKeyValues;
    function _dummyText(seed, length) {
        var text = '';
        for (var i = 0; i < length; i += seed.length) {
            text += seed;
        }
        return text.substring(0, length);
    }
    function dummyText(length) {
        if (length === void 0) { length = 50; }
        var seed = 'æ—¥æœ¬å›½æ°‘ã¯ã€æ­£å½“ã«é¸æŒ™ã•ã‚ŒãŸå›½ä¼šã«ãŠã‘ã‚‹ä»£è¡¨è€…ã‚’é€šã˜ã¦è¡Œå‹•ã—ã€ã‚ã‚Œã‚‰ã¨ã‚ã‚Œã‚‰ã®å­å­«ã®ãŸã‚ã«ã€è«¸å›½æ°‘ã¨ã®å”å’Œã«ã‚ˆã‚‹æˆæžœã¨ã€ã‚ãŒå›½å…¨åœŸã«ã‚ãŸã¤ã¦è‡ªç”±ã®ã‚‚ãŸã‚‰ã™æµæ²¢ã‚’ç¢ºä¿ã—ã€æ”¿åºœã®è¡Œç‚ºã«ã‚ˆã¤ã¦å†ã³æˆ¦äº‰ã®æƒ¨ç¦ãŒèµ·ã‚‹ã“ã¨ã®ãªã„ã‚„ã†ã«ã™ã‚‹ã“ã¨ã‚’æ±ºæ„ã—ã€ã“ã“ã«ä¸»æ¨©ãŒå›½æ°‘ã«å­˜ã™ã‚‹ã“ã¨ã‚’å®£è¨€ã—ã€ã“ã®æ†²æ³•ã‚’ç¢ºå®šã™ã‚‹ã€‚ãã‚‚ãã‚‚å›½æ”¿ã¯ã€å›½æ°‘ã®åŽ³ç²›ãªä¿¡è¨—ã«ã‚ˆã‚‹ã‚‚ã®ã§ã‚ã¤ã¦ã€ãã®æ¨©å¨ã¯å›½æ°‘ã«ç”±æ¥ã—ã€ãã®æ¨©åŠ›ã¯å›½æ°‘ã®ä»£è¡¨è€…ãŒã“ã‚Œã‚’è¡Œä½¿ã—ã€ãã®ç¦åˆ©ã¯å›½æ°‘ãŒã“ã‚Œã‚’äº«å—ã™ã‚‹ã€‚ã“ã‚Œã¯äººé¡žæ™®éã®åŽŸç†ã§ã‚ã‚Šã€ã“ã®æ†²æ³•ã¯ã€ã‹ã‹ã‚‹åŽŸç†ã«åŸºãã‚‚ã®ã§ã‚ã‚‹ã€‚ã‚ã‚Œã‚‰ã¯ã€ã“ã‚Œã«åã™ã‚‹ä¸€åˆ‡ã®æ†²æ³•ã€æ³•ä»¤åŠã³è©”å‹…ã‚’æŽ’é™¤ã™ã‚‹ã€‚';
        return _dummyText(seed, length);
    }
    sample.dummyText = dummyText;
    function dummyTextEn(length) {
        if (length === void 0) { length = 50; }
        var seed = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
        return _dummyText(seed, length);
    }
    sample.dummyTextEn = dummyTextEn;
    function measureTime(f) {
        var now = Date.now();
        f();
        return Date.now() - now;
    }
    sample.measureTime = measureTime;
    function eventLog(control, ignoredEvents, showArgs) {
        if (showArgs === void 0) { showArgs = false; }
        for (var eventName in control) {
            var event = control[eventName];
            if (event instanceof Object && event.hasOwnProperty('_handlers')) {
                if (ignoredEvents != undefined && ignoredEvents.indexOf(eventName) > -1) {
                    continue;
                }
                if (showArgs) {
                    control[eventName].addHandler(function (s, e) {
                        console.log(this.toString(), e);
                    }, eventName);
                }
                else {
                    control[eventName].addHandler(function (s, e) {
                        console.log(this.toString());
                    }, eventName);
                }
            }
        }
    }
    sample.eventLog = eventLog;
})(sample || (sample = {}));