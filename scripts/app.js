import {CustomGridEditor} from './CustomGridEditor.js';
var grid;

var columns=[
    {header:"ID", binding:"id"},
    {header:"Country", binding:"country"},
    {header:"Products", binding:"products"},
    {header:"Checked", binding:"checked"}
];

grid=new wijmo.grid.FlexGrid('#theGrid',{
    autoGenerateColumns:false,
    columns:columns,
    itemsSource:getData(10)
});

grid.columns[2].width=200;

function getProducts(){
    var products = 'Orange,Apple,Mango,Pear,Banana'.split(',');

    var tmp = [];
    for(var i=0; i<products.length; i++) {
        tmp.push({
            id: i,
            name: products[i]
        });
    }

    return tmp;
}

function getCountries() {
    var country = 'US,Germany,UK,Japan,Italy,Greece'.split(',');

    return country;
}

new CustomGridEditor(grid,"products",wijmo.input.MultiSelect,{
    showSelectAllCheckbox: true,
    itemsSource:getProducts(),
    displayMemberPath: 'name',
    selectedValuePath: 'name',
    checkedItemsChanged: function (s, e) {
        // console.log(s.checkedMemberPath);
    }
});

grid.formatItem.addHandler((s,e)=>{
	if(e.panel!=s.cells||e.editRange){
		return;
	}
	if(s.columns[e.col].binding!="products"){
		return;
	}
    var dataItem = s.rows[e.row].dataItem.products;
	var displayString = dataItem.reduce((acc,curr)=>{
		if(acc){
			return acc+","+curr.name;
		}else{
			return curr.name;
		}
    },"");

	e.cell.innerHTML=displayString;
});

function getData(count){
    // create some random data
    var countries = getCountries(),
        products = getProducts(),
        data = [];

    for (var i = 0; i < count; i++) {
        var condition=Boolean(Math.floor(Math.random()+.5));
        data.push({
            id: i+1,
            country: countries[i % countries.length],
            products: [products[0],products[1]],
            checked: condition
        });
    }
    return data;
}

// var ids = [1,3,5];
// var x = [
//             { id: 1, name: 'A' },
//             { id: 2, name: 'B' },
//             { id: 3, name: 'C' },
//             { id: 4, name: 'D' },
//             { id: 5, name: 'E' },
//             { id: 6, name: 'F' }
//         ];
// console.log(x.filter(function (item) {
//     return ids.indexOf(item.id) > -1;
// }));